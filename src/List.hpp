/*
 * NAME:        Sri Padala
 * Description: A Templated Doubly Linked List.  
 */

#ifndef DOUBLE_LINK_LIST_HPP
#define DOUBLE_LINK_LISH_HPP

// Necessary includes
using namespace std;
// CLASS DECLARATIONS
template<typename T>
class List {
public:
    // Default constructor
    List();
    // Constructor that takes first item
    List(T firstElem);
    // Copy constructor
    List(const List<T>& ListObj);
    // // Move constructor
    List(List<T>&& ListObj);
    // // Destructor
    ~List();
    // function front; l-value
    T front(){return(*begin());}
    // function front; return by value
    const T front()const{return(*begin());}
    // function back; l-value
    T back(){return(*(iterator(tail)));}
    // function back; return by value
    const T back()const{return(*(const_iterator(tail)));}

    class iterator;
    class reverse_iterator;
    class const_iterator;
    class const_reverse_iterator;
    // function begin
    iterator begin(){return(iterator(head));}
    // function begn; const_iterator overload
    const_iterator begin()const{return(const_iterator(head));}
    // function end
    iterator end(){return(iterator(tail->getNextLink()));}
    // function end; const_iterator overload
    const_iterator end() const{return(const_iterator(tail->getNextLink()));}
    // function rbegin
    reverse_iterator rbegin(){return(reverse_iterator(tail));}
    // // function rbegin; const_reverse_iterator overload
    const_reverse_iterator rbegin() const{return(const_reverse_iterator(tail));}
    // // function rend
    reverse_iterator rend(){
        return(reverse_iterator(head->getPreviousLink()));}
    // // function rend; const_reverse_iterator overload
    const_reverse_iterator rend() const{return(const_reverse_iterator(head->getPreviousLink()));}
    // // function cbegin
    const_iterator cbegin() const noexcept{return(const_iterator(head));}
    // // function cend{return(reverse_iterator(head->getNextLink()));}
    const_iterator cend() const noexcept{return(const_iterator(nullptr));}
    // // function crbegin
    const_reverse_iterator crbegin()const noexcept{return(const_reverse_iterator(tail));}
    // // function crend
    const_reverse_iterator crend()const noexcept{return(const_reverse_iterator(head->getPreviousLink()));}
    // function search; returns iterator            // throws std::invalid_argument
    iterator search(const T& Obj);
    // // function search; returns const_iterator      // throws std::invalid_argument
    const_iterator search(const T& Obj)const;
    // // function erase; returns iterator to one after what was erased    // throws std::invalid_argument
    iterator erase(T item);
    // // function erase; overload that takes an iterator to what should be erased // throws std::invalid_argument
    iterator erase(iterator place);
    // // function insert; takes an iterator to position where insertion occurs as well as item to insert, returns iterator to inserted item
    iterator insert(iterator place, const T& item);
    // // function push_back;
    void push_back(const T& item);
    // // function push_front;
    void push_front(const T& item);
    // function emplace; takes an iterator to position where insertion occurs as well as item constructor parameters, returns iterator to inserted item
    template <typename ...Args> iterator emplace(iterator place, Args&&... args);
    // // function emplace_back; takes item constructor parameters
    template <typename ...Args> void emplace_back (Args&&... args);
    // // function emplace_front; takes item constructor parameters
    template <typename ...Args> void emplace_front (Args&&... args);
    // // function size();
    unsigned int size()const noexcept{return(count);}
    // function empty();
    bool empty() const noexcept{return(size()==0);}
    // function copy assignment overload;
    List& operator = (const List<T>& item);
    // // function move assignment overload;
    List& operator = (List<T>&& item);
private:
    class Node;
    Node* head;
    Node* tail;
    unsigned int count;
    Node* searchHelper(iterator loc) const;
    // Whatever data or helper functions you deem necessary
};

template <typename T>
class List<T>::Node {
public:
    // Functions that you deem necessary
    Node(){}
    Node(T thedata,Node* previous,Node* next)
        :data(thedata),nextLink(next),previousLink(previous){}
    Node* getNextLink() const{return nextLink;}//getters.
    Node* getPreviousLink() const{return previousLink;}
    T getData() const {return data;}
    void setData(T theData){data = theData;}//setters.
    void setNextLink(Node* pointer){nextLink = pointer;}
    void setPreviousLink(Node* pointer){previousLink = pointer;}
private:
    T data;
    Node* nextLink;
    Node* previousLink;
    // Data that you deem necessary
    // It shall be a doubly linked list
};

template <typename T>
class List<T>::iterator {
public:
    friend class List<T>;
    // Only need one constructor
    iterator(Node* initial): current(initial){}
    // function prefix increment operator overload
    iterator& operator ++()
    {
        current = current->getNextLink();
        return(*this);
    }
    // function postfix increment operator overload
    const iterator operator++(int){
        iterator temp(current);
        current = current->getNextLink();
        return(temp);
    }
    // function prefix decrement operator overload
    iterator& operator --()
    {
        current = current->getPreviousLink();
        return(*this);
    }
    // function postfix decrement operator overload
    const iterator operator --(int){
        iterator temp(current);
        current = current->getPreviousLink();
        return(temp);
    }
    // function de-reference operator overlaod; l-value
    const T operator*()const{
        return(current->getData());
    }
    // function equivalency operator overload
    bool operator ==(const iterator& rside)const{
        return(current == rside.current);
    }
    // function inequivalency operator overload
    bool operator !=(const iterator& rside)const{
        return(current != rside.current);
    }
private:
    // Resource that iterator manages
    Node* current;
};

template <typename T>
class List<T>::reverse_iterator {
public:
    // See iterator definition
    friend class List<T>;
    // Only need one constructor
    reverse_iterator(Node* initial): rcurrent(initial){}
    // function prefix increment operator overload
    reverse_iterator& operator ++()
    {
        rcurrent = rcurrent->getPreviousLink();
        return(*this);
    }
    // function postfix increment operator overload
    const reverse_iterator operator++(int){
        iterator temp(rcurrent);
        rcurrent = rcurrent->getPreviousLink();
        return(temp);
    }
    // function prefix decrement operator overload
    reverse_iterator& operator --()
    {
        rcurrent = rcurrent->getNextLink();
        return(*this);
    }
    // function postfix decrement operator overload
    const reverse_iterator operator --(int){
        iterator temp(rcurrent);
        rcurrent = rcurrent->getNextLink();
        return(temp);
    }
    // function de-reference operator overlaod; l-value
    const T operator*()const{
        return(rcurrent->getData());
    }
    // function equivalency operator overload
    bool operator ==(const reverse_iterator& rside)const{
        return(rcurrent == rside.rcurrent);
    }
    // function inequivalency operator overload
    bool operator !=(const reverse_iterator& rside)const{
        return(rcurrent != rside.rcurrent);
    }

private:
    // See iterator definition
    Node* rcurrent;
};

template <typename T>
class List<T>::const_iterator {
public:
    friend class List<T>;
    // See iterator definition
    const_iterator(Node* initial): ccurrent(initial){}
    // function prefix increment operator overload
    const_iterator& operator ++()
    {
        ccurrent = ccurrent->getNextLink();
        return(*this);
    }
    // function postfix increment operator overload
    const const_iterator operator++(int){
        const_iterator temp(ccurrent);
        ccurrent = ccurrent->getNextLink();
        return(temp);
    }
    // function prefix decrement operator overload
    const_iterator& operator --()
    {
        ccurrent = ccurrent->getPreviousLink();
        return(*this);
    }
    // function postfix decrement operator overload
    const const_iterator operator --(int){
        const_iterator temp(ccurrent);
        ccurrent = ccurrent->getPreviousLink();
        return(temp);
    }
    // function de-reference operator overlaod; l-value
    const T operator*()const{
        return(ccurrent->getData());
    }
    // function equivalency operator overload
    bool operator ==(const const_iterator& rside)const{
        return(ccurrent == rside.ccurrent);
    }
    // function inequivalency operator overload
    bool operator !=(const const_iterator& rside)const{
        return(ccurrent != rside.ccurrent);
    }
private:
    // See iterator definition
    Node* ccurrent;
};

template <typename T>
class List<T>::const_reverse_iterator {
public:
    // See iterator definition
    friend class List<T>;
    // Only need one constructor
    const_reverse_iterator(Node* initial): crcurrent(initial){}
    // function prefix increment operator overload
    const_reverse_iterator& operator ++()
    {
        crcurrent = crcurrent->getPreviousLink();
        return(*this);
    }
    // function postfix increment operator overload
    const const_reverse_iterator operator++(int){
        const_reverse_iterator temp(crcurrent);
        crcurrent = crcurrent->getPreviousLink();
        return(temp);
    }
    // function prefix decrement operator overload
    const_reverse_iterator& operator --()
    {
        crcurrent = crcurrent->getNextLink();
        return(*this);
    }
    // function postfix decrement operator overload
    const const_reverse_iterator operator --(int){
        iterator temp(crcurrent);
        crcurrent = crcurrent->getNextLink();
        return(temp);
    }
    // function de-reference operator overlaod; l-value
    const T operator*()const{
        return(crcurrent->getData());
    }
    // function equivalency operator overload
    bool operator ==(const const_reverse_iterator& rside)const{
        return(crcurrent == rside.crcurrent);
    }
    // function inequivalency operator overload
    bool operator !=(const const_reverse_iterator& rside)const{
        return(crcurrent != rside.crcurrent);
    }
private:
    // See iterator definition
    Node* crcurrent;
};



// CLASS IMPLEMENTATIONS BELOW
template <typename T>
List<T>::List()
    :head(nullptr),tail(nullptr),count(0){}

template <typename T>
List<T>::List(T firstElem)
{
    head = tail  = new Node(firstElem,nullptr,nullptr);
    count = 1; 
}
template <typename T>
List<T>::List(const List<T>& ListObj)
    :head(nullptr),tail(nullptr),count(0)
{
    for( T x : ListObj ) {//push backs the element.
            push_back(x);
    }
}
template <typename T>
List<T>::List(List<T>&& ListObj){
    count = ListObj.count;//takes the ownership.
    head = ListObj.head;
    tail = ListObj.tail;
    ListObj.count = 0;
    ListObj.head = nullptr;
    ListObj.tail = nullptr;
}
template <typename T>
List<T>::~List(){
    Node * tmp = nullptr;
    while (head)
        {
        tmp = head;
        head = head->getNextLink();
        delete tmp;
    }
    head = tail = nullptr;
}
template <typename T>
void List<T>::push_back(const T& item){
    Node *newNode = new Node(item,nullptr,nullptr);
    if(!tail){//empty
        head = newNode;
    }else{//not empty
        newNode->setPreviousLink(tail);
        tail->setNextLink(newNode); 
    }
    tail = newNode;
    count++;
}

template <typename T>
void List<T>::push_front(const T& item){


    Node* newNode = new Node(item,nullptr,head);
    if(!tail){//empty
        head = newNode;
        tail = head;
    }else{//not empty
        head->setPreviousLink(newNode);
        head = newNode;
    }
    head = newNode;
    count++;
}

template <typename T>
typename List<T>::iterator List<T>::search(const T& Obj){
    Node* temp;
    temp=head;
    while(temp!=nullptr)
    {
        if(temp->getData()==Obj)
        {
            return temp;
            break;
        }
        temp=temp->getNextLink();
    }
    throw(std::invalid_argument("Nothing Found"));//if nothing is found till the end it throws an exception.

}
template <typename T>
typename List<T>::const_iterator List<T>::search(const T& Obj)const{

    Node* temp;
    temp=head;
    while(temp!=nullptr)
    {
        if(temp->getData()==Obj)
        {
            return temp;
            break;
        }
        temp=temp->getNextLink();
    }
    throw(std::invalid_argument("Nothing Found"));//if nothing is found till the end it throws an exception.

}
template <typename T>
typename List<T>::iterator List<T>::erase(T del){
    Node* temp;
    temp=head;
    if(head==tail)
    {
        if(head->getData()!=del)
        {
            std::cout<<"could not delete"<<std::endl;
        }
        head=nullptr;
        tail=nullptr;
        delete temp;
        return(iterator(nullptr));
    }
    if(head->getData()==del)
    {
        head=head->getNextLink();
        head->setPreviousLink(nullptr);
        delete temp;
        return (iterator(head));
    }
    else if(tail->getData()==del)
    {
        temp=tail;
        tail=tail->getPreviousLink();
        tail->setNextLink(nullptr);
        delete temp;
        return(iterator(tail));
    }
    while(temp->getData()!=del)
    {
        temp=temp->getNextLink();
        if(temp==nullptr)
        {
            throw(std::invalid_argument("Nothing Found"));//if nothing is found till the end it throws an exception.
            return(iterator(nullptr));
        }
    }
    Node* temp2 = temp->getNextLink();
    (temp->getNextLink())->setPreviousLink(temp->getPreviousLink());
    (temp->getPreviousLink())->setNextLink(temp->getNextLink());
    delete temp;
    count--;
    return(iterator(temp2));
}
template <typename T>
typename List<T>::iterator List<T>::erase(iterator place){
    Node* temp;
    temp = head;
    if(head == tail)
    {
        if(head !=place.current)
        {
            cout<<"erasing the only item in the list"<<endl;
            return(iterator(nullptr));
        }
        head=nullptr;
        tail=nullptr;
        delete temp;
        count--;
        return(iterator(nullptr));
    }
    if(head == place.current)
    {
        head = head->getNextLink();
        head->setPreviousLink(nullptr);
        delete temp;
        count--;
        return(iterator(head));
    }else if(tail == place.current){
        temp = tail;
        tail = tail->getPreviousLink();
        tail->setNextLink(nullptr);
        delete temp;
        count--;
        return(iterator(tail));
    }
    while(temp!=place.current)
    {
        temp=temp->getNextLink();
        if(temp==nullptr)
        {
            throw(std::invalid_argument("Nothing Found"));
            return(iterator(nullptr));
        }
    }
    Node* temp2 = temp->getNextLink();
    (temp->getNextLink())->setPreviousLink(temp->getPreviousLink());
    (temp->getPreviousLink())->setNextLink(temp->getNextLink());
    delete temp;
    count--;
    return(iterator(temp2));

}

template <typename T>
typename List<T>::iterator List<T>::insert(iterator place , const T& item){
   
   
    Node *in = new Node(item,nullptr,nullptr);

    if (head) {//if there is a node.
        Node *pos = searchHelper(place);
        Node *prev = head;

        while (prev->getNextLink() != pos && prev->getNextLink() != nullptr) {
            prev = prev->getNextLink();
        }
        if(head == pos){
            in->setNextLink(pos);
            pos->setPreviousLink(in);
            in->setPreviousLink(nullptr);
            head = in;
            count++;
        }else if(tail == pos){
            in->setNextLink(pos);
            prev->setNextLink(in);
            in->setPreviousLink(prev);
            pos->setPreviousLink(in);
            count++;
        }else{
            in->setNextLink(pos);
            prev->setNextLink(in);
            pos->setPreviousLink(in);
            in->setPreviousLink(prev);
            count++;
        }
    }else{//no node.
        head = tail = in;
        count++;
    }
    count++;
    return iterator(in);
}


template <typename T>
typename List<T>::Node* List<T>::searchHelper(typename List<T>::iterator loc) const
{
    Node *tmp = head;//From the class lecture to find the node.

    while (tmp != loc.current && tmp != nullptr) {
        if (tmp == loc.current)
            break;
        else
            tmp = tmp->getNextLink();
    }

    return tmp;
}

template <typename T>
List<T>& List<T>::operator = (const List<T>& item){//copy assignment.

    Node  *newNode = new Node; 
	Node *current; 

		if (item.head == nullptr)//no node.
		{
			head = nullptr;
			tail = nullptr;
			count = 0;
		}else
		{
			current = item.head; 
			count = item.count;
            head->setData(current->getData());
            head->setNextLink(current->getNextLink());
            head->setPreviousLink(current->getPreviousLink());

	        tail = head;

			current = current->getNextLink(); 

	            while (current != nullptr)
	            {
	        	newNode = new Node; 
                newNode->setData(current->getData());
		        newNode->setNextLink(current->getNextLink());
                newNode->setPreviousLink(current->getPreviousLink());
	        	tail->setNextLink(newNode);

	        	tail = newNode;
	        	current = current->getNextLink();
	            }
		}
        return *this;
}
template <typename T>
List<T>& List<T>::operator =(List<T>&& item){//move assignment.
    if(head != item.head && tail != item.tail){
        count = 0;
        head = nullptr;
        tail = nullptr;
        count = item.count;
        head = item.head;
        tail = item.tail;
        item.count = 0;
        item.head = item.tail = nullptr;  
    }
    return *this;        
}
template <typename T>
template <typename ...Args>
void List<T>::emplace_back(Args&&... args){
    push_back(std::move(T(std::forward<Args>(args) ...)));
}

template <typename T>
template <typename ...Args>
void List<T>::emplace_front(Args&&... args){
    push_front(std::move(T(std::forward<Args>(args) ...)));
}
template <typename T>
template <typename ...Args>
typename List<T>::iterator List<T>::emplace(iterator place, Args&&... args){
    iterator temp(nullptr);
    temp = insert(place,std::move(T(std::forward<Args>(args) ...)));
    return(temp);
}
#endif
